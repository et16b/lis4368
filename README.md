> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 - Advanced Web Application Development

## Eli Torres

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - MYSQL installation
    - Compiling and using servlets
    - Database connectivity using servlets

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create database for web application
    - Create ERD
    - Insert 10 records

4.  [P1 README.md](p1/README.md "My P1 README.md file")
    - Basic client-side validation
    - Add form controls
    - Add jQuery validation and regular expressions

5.  [A4 README.md](a4/README.md "My A4 README.md file")
    - Basic server-side validation
    - Add form controls
    - Add screenshots of server-side validation working

6.  [A5 README.md](a5/README.md "My A5 README.md file")
    - Basic server-side validation
    - Add associated database entry based on user input
    - Add screenshots of server-side validation and database entry working

6.  [P2 README.md](p2/README.md "My P2 README.md file")
    - MVC framework using basic server-side and client-side validation
    - Complete CRUD functionality
    - Add result screen showing list of customer entries
