> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Eli Torres

### P1 Requirements:

*Sub-Heading:*

1. Descriptions
2. Screenshots
3. Links

#### README.md file should include the following items:

* Add form controls to match attributes of customer entity
* Add jQuery validation and regular expressions -- as per the entity attribute requirements
* Screenshots of basic client-side validation working

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of Main Page*:

![Main_Page](img/Main_Page.PNG "Main Page")

*Screenshot of P1 Failed Validation*:

![Failed_Validation](img/Failed_Validation.PNG "Failed Client-Side Validation")

*Screenshot of P1 Passed Validation*:

![Passed_Validation](img/Passed_Validation.PNG "Passed Client-Side Validation")



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/et16b/bitbucketstationlocations/ "Bitbucket Station Locations")

*Lis4368 Repo link*
[A1 Lis4368 Repo Link](https://bitbucket.org/et16b/lis4368)

*Link to local Lis4368 web app:*
[A1 Lis4368 web app](http://localhost:9999/lis4368/)
