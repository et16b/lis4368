> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Eli Torres

### A2 Requirements:

*Sub-Heading:*

1. Descriptions
2. Screenshots
3. Links

#### README.md file should include the following items:

* http://localhost:9999/hello (Displays directory! It should not! Needs index.html) 
* http://localhost:9999/hello/HelloHome.html (Rename "HelloHome.html" to "index.html" so that users cannot see your files!) 
* http://localhost:9999/hello/sayhello (invokes HelloServlet) 
* http://localhost:9999/hello/querybook.html
* http://localhost:9999/hello/sayhi (invokes AnotherHelloServlet) 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of running http://localhost:9999/hello without index.html*:

![Hello Screenshot](img/hello_no_index.PNG)

*Screenshot of running http://localhost:9999/hello/index.html with index.html file*:

![Hello with index Screenshot](img/hello.PNG)

*Screenshot of running http://localhost:9999/hello/sayhello*:

![Hello with hello Screenshot](img/sayhello.PNG)

*Screenshot of running http://localhost:9999/hello/querybook.html*:

![Hello with query book Screenshot](img/query_selected.PNG)

*Screenshot of running query results*:

![Hello with query results Screenshot](img/query_results.PNG)

*Screenshot of running http://localhost:9999/hello/sayhi*:

![Hello with hi Screenshot](img/sayhi.PNG)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/et16b/bitbucketstationlocations/ "Bitbucket Station Locations")

*Lis4368 Repo link*
[A1 Lis4368 Repo Link](https://bitbucket.org/et16b/lis4368)

*Link to local Lis4368 web app:*
[A1 Lis4368 web app](http://localhost:9999/lis4368/)
