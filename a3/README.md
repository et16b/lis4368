> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Eli Torres

### A2 Requirements:

*Sub-Heading:*

1. Descriptions
2. Screenshots
3. Links

#### README.md file should include the following items:

* Entity Relationship Diagram (ERD)
* Include data (at least 10 records each table) 
* Link to the following files: a3.mwb and a3.sql  

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot A3 ERD*:

![A3 ERD](img/a3.png "ERD based upon A3 Requirements")

*A3 docs: a3.mwb and a3.sql*:

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/a3.sql "A3 SQL Script")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/et16b/bitbucketstationlocations/ "Bitbucket Station Locations")

*Lis4368 Repo link*
[A1 Lis4368 Repo Link](https://bitbucket.org/et16b/lis4368)

*Link to local Lis4368 web app:*
[A1 Lis4368 web app](http://localhost:9999/lis4368/)
