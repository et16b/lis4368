> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Eli Torres

### A5 Requirements:

*Sub-Heading:*

1. Descriptions
2. Screenshots
3. Links

#### README.md file should include the following items:

* Create a basic server-side validation
* Prepared statements to help prevent SQL injection
* JSTL to prevent XSS. Also adds insert functionality to A4
* Pass valid data to associated database

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:


*Screenshot of Valid User Form Entry and Passed Validation*:

![Entry_And_Passed_Validation](img/Data_And_Passed.PNG "User Entry and Passed Validation")

*Screenshot of Associated Database Entry*:

![Database_Entry](img/Database.PNG "Database Entry")



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/et16b/bitbucketstationlocations/ "Bitbucket Station Locations")

*Lis4368 Repo link*
[A1 Lis4368 Repo Link](https://bitbucket.org/et16b/lis4368)

*Link to local Lis4368 web app:*
[A1 Lis4368 web app](http://localhost:9999/lis4368/)
