> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Eli Torres

### P2 Requirements:

*Sub-Heading:*

1. Descriptions
2. Screenshots
3. Links

#### README.md file should include the following items:

* MVC framework using basic client-side and server-side validation
* Complete CRUD Validation
* Screenshots of validation, modification, and deletion working

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of Valid Entry and Passed Validation Page*:

![Valid_and_Passed](img/valid_and_passed.PNG "Valid Entry and Passed Page")

*Screenshot of Display Data Page*:

![Display_Data](img/display_data.PNG "Display Data Page")

*Screenshot of Modify Form Page*:

![Modify](img/modify.PNG "Modify Form Page")

*Screenshot of Modified Data Page*:

![Modified_Page](img/modify_result.PNG "Modified Data Page")

*Screenshot of Delete Warning Message*:

![Delete_Warning](img/delete_warning.PNG "Delete Warning Page")

*Screenshot of Associated Database Changes*:

![Database_Changes](img/database_changes.PNG "Database Changes")



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/et16b/bitbucketstationlocations/ "Bitbucket Station Locations")

*Lis4368 Repo link*
[A1 Lis4368 Repo Link](https://bitbucket.org/et16b/lis4368)

*Link to local Lis4368 web app:*
[A1 Lis4368 web app](http://localhost:9999/lis4368/)
