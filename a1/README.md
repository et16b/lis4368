> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Eli Torres

### A1 Requirements:

*Sub-Heading:*

1. Descriptions
2. Screenshots
3. Links

#### README.md file should include the following items:

* Screenshot of running java Hello
* Screenshot of running http://localhost:9999
* git commands with short descriptions 
* Bitbucket repo links for this assignment and Bitbucket
*Link to local Lis4368 web app

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - creates a new repository
2. git status - displays state of working directory to the Git staging area
3. git add - moves changes from the working directory to the Git staging area
4. git commit - takes the staged snapshot and commits it to the project history
5. git push - used to upload local repository content to remote repository
6. git pull - used to fetch and download content from remote repository and update local repository to match that content 
7. git branch - list branches

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.PNG)

*Screenshot of running http://localhost:9999*:

![Android Studio Installation Screenshot](img/tomcat.PNG)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/et16b/bitbucketstationlocations/ "Bitbucket Station Locations")

*Lis4368 Repo link*
[A1 Lis4368 Repo Link](https://bitbucket.org/et16b/lis4368)

*Link to local Lis4368 web app:*
[A1 Lis4368 web app](http://localhost:9999/lis4368/)
