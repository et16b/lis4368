> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Eli Torres

### A4 Requirements:

*Sub-Heading:*

1. Descriptions
2. Screenshots
3. Links

#### README.md file should include the following items:

* Create a basic server-side validation
* Example of Model-View-Controller
* Screenshots of basic server-side validation working

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:


*Screenshot of Failed Validation*:

![Failed_Validation](img/failed_validation.PNG "Failed Client-Side Validation")

*Screenshot of Passed Validation*:

![Passed_Validation](img/passed_validation.PNG "Passed Client-Side Validation")



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/et16b/bitbucketstationlocations/ "Bitbucket Station Locations")

*Lis4368 Repo link*
[A1 Lis4368 Repo Link](https://bitbucket.org/et16b/lis4368)

*Link to local Lis4368 web app:*
[A1 Lis4368 web app](http://localhost:9999/lis4368/)
